package com.digilex.infordoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InfordocTwoApplication {

	public static void main(String[] args) {
		SpringApplication.run(InfordocTwoApplication.class, args);
	}
}
