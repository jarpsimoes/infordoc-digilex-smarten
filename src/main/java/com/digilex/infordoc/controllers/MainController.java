package com.digilex.infordoc.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by joaosimoes on 19/04/2017.
 */
@Controller
public class MainController {

    @RequestMapping(value="/")
    public String index(){
        return "index";
    }

}
