package com.digilex.infordoc.database.repositories;

import com.digilex.infordoc.database.tables.Document;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by joaosimoes on 14/04/2017.
 */
public interface DocumentRepository extends CrudRepository<Document, Long> {

}
