package com.digilex.infordoc.database.tables;

import lombok.Data;
import javax.persistence.Id;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import java.util.Date;

/**
 * Created by joaosimoes on 14/04/2017.
 */
@Data
@Entity
public class Document {

    private @Id @GeneratedValue Long id;
    private Date date_created;
    private Date date_updated;
    private boolean deleted;
    private String title;
    private String subtitle;
    private String resume_txt;
    private String content_txt;

    public Document(){}

    public Document(Date date_created, Date date_updated, boolean deleted, String title, String subtitle, String resume_txt, String content_txt) {
        this.date_created = date_created;
        this.date_updated = date_updated;
        this.deleted = deleted;
        this.title = title;
        this.subtitle = subtitle;
        this.resume_txt = resume_txt;
        this.content_txt = content_txt;
    }
}
