var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    debug: true,
    devtool: 'inline-source-map',
    noInfo: false,
    entry: {
        main: './src/js_modules/main.js'
    },
    target: 'web',
    output: {
        filename: '/app/target/resources/js/app.js'
    },
    externals: {
        // require("jquery") is external and available
        //  on the global var jQuery
        "jquery": "jQuery"
    },
    module: {
        loaders: [
            {test: /\.js?$/, loader: 'babel',
                query: {
                    presets: ['es2015', 'react']
                }
            },
            {test: /\.scss$/, loader: ExtractTextPlugin.extract('css!sass')},
            {test: /\.js$/, exclude: /node_modules/, loaders: ['eslint']}
        ]
    },
    plugins: [
        new ExtractTextPlugin('../src/main/resources/static/css/app.css', {
            allChunks: true
        }),

    ],
    watchOptions: {
        poll: true
    }
}
