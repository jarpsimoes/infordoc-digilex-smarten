'use strict';

const React = require('react');
const ReactDOM = require('react-dom');
const client = require('./client');

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {documents: []};
    }

    componentDidMount() {
        client({method: 'GET', path: '/api/documents'}).done(response => {
            this.setState({documents: response.entity._embedded.documents});
    });

    }
    render() {
        return (
            <DocumentList documents={this.state.documents}/>
        )
    }
}

// tag::document-list[]
class DocumentList extends React.Component{
    render() {
        var documents = this.props.documents.map(document =>
            <Document key={document._links.self.href} document={document}/>
        );
        return (
            <table>
                <tbody>
                <tr>
                    <th>First Names</th>
                    <th>Last Name</th>
                    <th>Description</th>
                </tr>
                {documents}
                </tbody>
            </table>
        )
    }
}
// end::document-list[]

// tag::document[]
class Document extends React.Component{
    render() {
        return (
            <tr>
                <td>{this.props.document.title}</td>
                <td>{this.props.document.subtitle}</td>
                <td>{this.props.document.resume_txt}</td>
            </tr>
        )
    }
}
// end::document[]

// tag::render[]
ReactDOM.render(
    <App />,
    document.getElementById('react')
)
// end::render[]