import Jasmine from 'jasmine';
var jasmine = new Jasmine();

jasmine.loadConfigFile('./src/spec/support/jasmine.json');

jasmine.execute();
